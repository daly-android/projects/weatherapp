package com.daly.weatherapp.ui.theme

import androidx.compose.ui.graphics.Color

val DarkBlue = Color(0xFF1B3B5A)
val DeepBlue = Color(0xFF102840)

val Diamond = Color(0xFFC4E2FF)
val LapisLazuli = Color(0xFF24609B)
val Dark = Color(0xFF000000)
