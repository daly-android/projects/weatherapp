package com.daly.weatherapp.core.impl

import android.content.Context
import androidx.annotation.StringRes
import com.daly.weatherapp.core.ResourcesRepository

class ResourcesRepositoryImpl(private val appContext: Context) : ResourcesRepository {

    override fun fetchString(@StringRes id: Int, vararg args: Any?): String = appContext.getString(id, *args)
}
