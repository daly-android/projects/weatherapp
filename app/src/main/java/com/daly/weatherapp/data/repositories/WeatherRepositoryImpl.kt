package com.daly.weatherapp.data.repositories

import com.daly.weatherapp.data.mappers.toWeatherInfo
import com.daly.weatherapp.data.remote.WeatherApi
import com.daly.weatherapp.domain.models.Latitude
import com.daly.weatherapp.domain.models.Longitude
import com.daly.weatherapp.domain.models.WeatherInfo
import com.daly.weatherapp.domain.repositories.WeatherRepository
import com.daly.weatherapp.domain.utils.Resource
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(
    private val api: WeatherApi
) : WeatherRepository {
    override suspend fun getWeatherData(lat: Latitude, long: Longitude): Resource<WeatherInfo> {
        return try {
            Resource.Success(
                data = api.getWeatherData(
                    lat = lat.value,
                    long = long.value
                ).toWeatherInfo()
            )
        } catch (e: Exception) {
            e.printStackTrace()
            Resource.Error(e.message ?: "An unknown error occurred")
        }
    }
}
