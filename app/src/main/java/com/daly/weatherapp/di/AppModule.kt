package com.daly.weatherapp.di

import android.app.Application
import com.daly.weatherapp.BuildConfig
import com.daly.weatherapp.core.DispatcherService
import com.daly.weatherapp.core.ResourcesRepository
import com.daly.weatherapp.core.impl.DispatcherServiceImpl
import com.daly.weatherapp.core.impl.ResourcesRepositoryImpl
import com.daly.weatherapp.data.remote.WeatherApi
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideMyApi(): WeatherApi {
        return Retrofit.Builder()
            .baseUrl("https://api.open-meteo.com/")
            .addConverterFactory(MoshiConverterFactory.create())
            .client(OkHttpClient.Builder().addInterceptor(buildHttpLoggingInterceptor()).build())
            .build()
            .create()
    }

    private fun buildHttpLoggingInterceptor() = HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
    }

    @Provides
    @Singleton
    fun provideFusedLocationProviderClient(app: Application): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(app)
    }

    @Provides
    @Singleton
    fun provideResourcesRepository(app: Application): ResourcesRepository {
        return ResourcesRepositoryImpl(app)
    }

    @Provides
    @Singleton
    fun provideDispatcherProvider(): DispatcherService {
        return DispatcherServiceImpl()
    }
}
