package com.daly.weatherapp.domain.models

@JvmInline
value class Latitude(val value: Double)
