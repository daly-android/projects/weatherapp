package com.daly.weatherapp.domain.repositories

import com.daly.weatherapp.domain.models.Latitude
import com.daly.weatherapp.domain.models.Longitude
import com.daly.weatherapp.domain.models.WeatherInfo
import com.daly.weatherapp.domain.utils.Resource

interface WeatherRepository {
    suspend fun getWeatherData(lat: Latitude, long: Longitude): Resource<WeatherInfo>
}
