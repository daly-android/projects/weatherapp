package com.daly.weatherapp.domain.location

import android.location.Location

interface LocationTracker {
    suspend fun getCurrentLocation(): Location?

    fun isGpsEnabled(): Boolean
}
