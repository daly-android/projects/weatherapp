package com.daly.weatherapp.domain.models

@JvmInline
value class Longitude(val value: Double)
