package com.daly.weatherapp.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.daly.weatherapp.R
import com.daly.weatherapp.presentation.states.WeatherUiState
import com.daly.weatherapp.presentation.utils.DIGITS_ONLY_HOUR_MINUTES
import java.time.format.DateTimeFormatter
import kotlin.math.roundToInt

@Composable
fun WeatherCard(
    state: WeatherUiState.Success,
    modifier: Modifier = Modifier,
    backgroundColor: Color
) {
    state.weatherInfo?.currentWeatherData?.let { data ->
        Card(
            colors = CardDefaults.cardColors().copy(containerColor = backgroundColor),
            shape = RoundedCornerShape(10.dp),
            modifier = modifier.padding(16.dp)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = stringResource(
                        id = R.string.weather_time_now_format,
                        data.time.format(
                            DateTimeFormatter.ofPattern(DIGITS_ONLY_HOUR_MINUTES)
                        )
                    ),
                    modifier = Modifier.align(Alignment.End),
                    color = MaterialTheme.colorScheme.onSurface
                )
                Spacer(modifier = Modifier.height(16.dp))
                Image(
                    painter = painterResource(id = data.weatherType.iconRes),
                    contentDescription = null,
                    modifier = Modifier.width(200.dp)
                )
                Spacer(modifier = Modifier.height(16.dp))
                Text(
                    text = "${data.temperatureCelsius}°C",
                    fontSize = 50.sp,
                    color = MaterialTheme.colorScheme.onSurface
                )
                Spacer(modifier = Modifier.height(16.dp))
                Text(
                    text = data.weatherType.weatherDesc,
                    fontSize = 20.sp,
                    color = MaterialTheme.colorScheme.onSurface
                )
                Spacer(modifier = Modifier.height(32.dp))
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    WeatherDataDisplay(
                        value = data.pressure.roundToInt(),
                        unit = stringResource(id = R.string.weather_pressure_unit),
                        icon = ImageVector.vectorResource(id = R.drawable.ic_pressure),
                        iconTint = MaterialTheme.colorScheme.onSurface,
                        textStyle = TextStyle(color = MaterialTheme.colorScheme.onSurface)
                    )
                    WeatherDataDisplay(
                        value = data.humidity.roundToInt(),
                        unit = stringResource(id = R.string.weather_humidity_unit),
                        icon = ImageVector.vectorResource(id = R.drawable.ic_drop),
                        iconTint = MaterialTheme.colorScheme.onSurface,
                        textStyle = TextStyle(color = MaterialTheme.colorScheme.onSurface)
                    )
                    WeatherDataDisplay(
                        value = data.windSpeed.roundToInt(),
                        unit = stringResource(id = R.string.weather_wind_speed_unit),
                        icon = ImageVector.vectorResource(id = R.drawable.ic_wind),
                        iconTint = MaterialTheme.colorScheme.onSurface,
                        textStyle = TextStyle(color = MaterialTheme.colorScheme.onSurface)
                    )
                }
            }
        }
    }
}
