package com.daly.weatherapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daly.weatherapp.R
import com.daly.weatherapp.core.DispatcherService
import com.daly.weatherapp.core.ResourcesRepository
import com.daly.weatherapp.domain.location.LocationTracker
import com.daly.weatherapp.domain.models.Latitude
import com.daly.weatherapp.domain.models.Longitude
import com.daly.weatherapp.domain.repositories.WeatherRepository
import com.daly.weatherapp.domain.utils.Resource
import com.daly.weatherapp.presentation.states.WeatherUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository,
    private val locationTracker: LocationTracker,
    private val resourcesRepository: ResourcesRepository,
    dispatcherService: DispatcherService
) : ViewModel() {

    private val _state = MutableStateFlow<WeatherUiState>(WeatherUiState.Loading)
    val state: StateFlow<WeatherUiState> = _state.stateIn(
        initialValue = WeatherUiState.Loading,
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000)
    )

    init {
        viewModelScope.launch(dispatcherService.io) {
            locationTracker.getCurrentLocation()?.let { location ->
                when (val result = weatherRepository.getWeatherData(Latitude(location.latitude), Longitude(location.longitude))) {
                    is Resource.Error -> {
                        _state.value = WeatherUiState.Error(result.message)
                    }
                    is Resource.Success -> {
                        _state.value = WeatherUiState.Success(result.data)
                    }
                }
            } ?: run {
                _state.value = WeatherUiState.Error(resourcesRepository.fetchString(R.string.weather_cannot_get_location_error))
            }
        }
    }
}
