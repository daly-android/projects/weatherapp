package com.daly.weatherapp.presentation.states

import com.daly.weatherapp.domain.models.WeatherInfo

sealed interface WeatherUiState {
    data class Success(val weatherInfo: WeatherInfo?) : WeatherUiState
    data object Loading : WeatherUiState
    data class Error(val message: String?) : WeatherUiState
}
