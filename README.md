## Weather App

## Overview
This is basic application to show the weather info according to user location.

It detect user location using "Play Services Location" library and fetch weather info for this location.

It contains one principle screen that show all the weather info : temperature, pressure, wind speed and humidity.

It also provide the hourly temperature for today.


|                                                                                             |                                                                                               |
|:-------------------------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------:|
| <img width="1604" alt="enable_location_dark_mode" src="docs/enable_location_dark_mode.png"> | <img width="1604" alt="enable_location_light_mode" src="docs/enable_location_light_mode.png"> |
|         <img width="1604" alt="weather_dark_mode" src="docs/weather_dark_mode.png">         |         <img width="1604" alt="weather_light_mode" src="docs/weather_light_mode.png">         |

## Technical stack :
# 100% Kotlin & Jetpack Compose
# Architecture : MVVM and clean
# Dependency injection : Dagger-Hilt
# Build configuration language : Kotlin DSL
# Location : Play services location
